import io
import os
import shlex
import subprocess
import tempfile
from pathlib import Path

import pandas as pd

import kmbio.PDB
from kmtools import structure_tools
from tkpod.plugins.modeller import Modeller


def get_interaction_dataset(structure, bioassembly_id=False, r_cutoff=5):
    """Copied from "datapkg/pdb-analysis/notebooks/extract_pdb_interactions.ipynb"
    """
    interactions = structure_tools.get_interactions(structure, r_cutoff=r_cutoff, interchain=False)
    interactions_core, interactions_interface = structure_tools.process_interactions(interactions)
    interactions_core_aggbychain = structure_tools.process_interactions_core(
        structure, interactions_core
    )
    # Not neccessary to drop duplicates in our cases
    # interactions_core, interactions_core_aggbychain = structure_tools.drop_duplicates_core(
    #     interactions_core, interactions_core_aggbychain
    # )
    return interactions_core, interactions_core_aggbychain


def get_pdb_interactions(row, pdb_ffindex_path: Path):
    structure_id, _, chain_id = row.template_id.partition("_")
    structure_url = f"ff://{pdb_ffindex_path}?{structure_id.lower()}.cif.gz"

    with kmbio.PDB.open_url(structure_url) as fin:
        structure = kmbio.PDB.MMCIFParser().get_structure(fin)
        fin.seek(0)
        structure_dict = kmbio.PDB.mmcif2dict(fin)

    chain_sequence = structure_tools.get_chain_sequence(structure[0][chain_id])

    if isinstance(structure_dict["_entity_poly.pdbx_strand_id"], str):
        construct_sequence = structure_dict["_entity_poly.pdbx_seq_one_letter_code_can"]
    else:
        seq_idx = [
            i
            for i, v in enumerate(structure_dict["_entity_poly.pdbx_strand_id"])
            if chain_id in v.split(",")
        ][0]
        construct_sequence = structure_dict["_entity_poly.pdbx_seq_one_letter_code_can"][seq_idx]

    try:
        offset = construct_sequence.index(chain_sequence[:10])
    except ValueError:
        offset = 0

    target_1 = structure_tools.DomainTarget(
        0,
        chain_id,
        row.template_ali,
        row.template_start - offset,
        row.template_end - offset,
        row.query_ali,
    )

    modeller_data = Modeller.build(
        structure_url, use_auth_id=True, bioassembly_id=False, use_strict_alignment=False
    )

    # ---

    structure_fixed = kmbio.PDB.load(
        modeller_data.structure_file, bioassembly_id=modeller_data.bioassembly_id
    )
    structure_fixed_cut, alignment = structure_tools.prepare_for_modeling(
        structure_fixed, [target_1], strict=modeller_data.use_strict_alignment
    )

    interactions_core, interactions_core_aggbychain = get_interaction_dataset(structure_fixed_cut)
    return interactions_core, interactions_core_aggbychain


def get_homology_model_interactions(row):
    structure_data = row.structure_text
    fh = io.StringIO()
    fh.write(structure_data)
    fh.seek(0)

    parser = kmbio.PDB.PDBParser()
    structure = parser.get_structure(fh)
    structure.id = row.unique_id
    interactions_core, interactions_core_aggbychain = get_interaction_dataset(structure)
    return interactions_core, interactions_core_aggbychain


def run_rosetta_relax(input_file, temp_path, row):
    output_file = temp_path.joinpath("relax.sc")
    if output_file.is_file():
        output_file.unlink()

    # -relax:coord_constrain_sidechains \
    system_command = f"""\
{os.environ['ROSETTA_BIN']}/relax.static.linuxgccrelease \
-in:file:s '{input_file}' \
-out:file:scorefile '{output_file}' \
-overwrite \
-ignore_unrecognized_res \
--relax:constrain_relax_to_start_coords \
--relax:ramp_constraints false \
-nstruct 3
"""
    proc = subprocess.run(
        shlex.split(system_command),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
        cwd=temp_path,
    )
    if proc.returncode != 0:
        print(proc.stdout)
        print(proc.stderr)
        raise Exception("Rosetta relax crashed!")

    relax_df = pd.read_csv(temp_path.joinpath("relax.sc"), sep=" +", engine="python", skiprows=1)
    del relax_df["SCORE:"]
    return relax_df


def run_rosetta_score(input_file, temp_path, row):
    input_files = [input_file] + list(input_file.parent.glob(input_file.stem + "_*.pdb"))

    output_file = temp_path.joinpath("score_jd2.sc")
    if output_file.is_file():
        output_file.unlink()

    system_command = f"""\
{os.environ['ROSETTA_BIN']}/score_jd2.static.linuxgccrelease \
-in:file:s {' '.join([f"'{f}'" for f in input_files])} \
-out:file:scorefile '{output_file}' \
-overwrite \
"""
    proc = subprocess.run(
        shlex.split(system_command),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
        cwd=temp_path,
        check=True,
    )
    if proc.returncode != 0:
        print(proc.stdout)
        print(proc.stderr)
        raise Exception("Rosetta score crashed!")

    score_jd2_df = pd.read_csv(
        temp_path.joinpath("score_jd2.sc"), sep=" +", engine="python", skiprows=1
    )
    del score_jd2_df["SCORE:"]
    return score_jd2_df


def get_rosetta_results(row):
    with tempfile.TemporaryDirectory() as temp_dir:
        temp_path = Path(temp_dir)
        input_file = temp_path.joinpath(row.unique_id + ".pdb")
        with input_file.open("wt") as fout:
            fout.write(row.structure_text)
        relax_df = run_rosetta_relax(input_file, temp_path, row)  # noqa
        score_jd2_df = run_rosetta_score(input_file, temp_path, row)
    best_rows = score_jd2_df[score_jd2_df["total_score"] == score_jd2_df["total_score"].min()]
    rosetta_results = dict(best_rows.mean())
    return rosetta_results
