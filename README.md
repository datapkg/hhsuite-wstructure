# hhsuite-wstructure

[![docs](https://img.shields.io/badge/docs-v0.1-blue.svg)](https://datapkg.gitlab.io/hhsuite-wstructure/ITlpVqisExgIYttfKpU0KjMiyMOyNdQgST7v0ANnZ67fovDDXqIRDsBE90P6rjaL)
[![build status](https://gitlab.com/datapkg/hhsuite-wstructure/badges/master/build.svg)](https://gitlab.com/datapkg/hhsuite-wstructure/commits/master/)

Databases from the hh-suite server augmented with PDB structures (in the case of pdb70) and adjacency matrices.

## Notebooks
